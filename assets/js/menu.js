$(document).on('mouseenter', '#gnb > nav > ul > li', function(e){
	$('div.gnb_overlay').addClass('show');
	$(this).parent().addClass('open');
	$('#gnb > nav > ul > li').removeClass('open');
	if( $('#gnb > nav > ul > li .shown').length > 0 ){
		$(this).children('div').show();
	}else{
		$(this).children('div').addClass('animate');
	}
	$('#gnb > nav > ul > li .shown').removeClass('shown');
	$(this).children('div').addClass('shown').addClass('animate');
	$(this).addClass('open');
});
$(document).on('mouseleave', '#gnb > nav', function(e){
	$('div.gnb_overlay').removeClass('show');
	$(this).find('div').removeClass('shown');
	$('#gnb > nav > ul > li').removeClass('open');
	$('#gnb > nav > ul').removeClass('open');
});