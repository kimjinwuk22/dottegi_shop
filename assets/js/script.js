$(document).on('mouseenter', '#gnb > nav > ul > li.dropdown', function(e){
  $('div.gnb_overlay').addClass('show');
  $(this).parent().addClass('open');
  $('#gnb > nav > ul > li.dropdown').removeClass('open');
  if( $('#gnb > nav > ul > li.dropdown .shown').length > 0 ){
    $(this).children('div').show();
  }else{
    $(this).children('div').addClass('animate');
  }
  $('#gnb > nav > ul > li.dropdown .shown').removeClass('shown');
  $(this).children('div').addClass('shown').addClass('animate');
  $(this).addClass('open');
  $('div.gnb_overlay').addClass('show');
  $(this).parent().addClass('open');
  $('#gnb > nav > ul > li.dropdown').removeClass('open');
  if( $('#gnb > nav > ul > li.dropdown .shown').length > 0 ){
    $(this).children('div').show();
  }else{
    $(this).children('div').addClass('animate');
  }
  $('#gnb > nav > ul > li.dropdown .shown').removeClass('shown');
  $(this).children('div').addClass('shown').addClass('animate');
  $(this).addClass('open');
});
$(document).on('mouseleave', '#gnb > nav', function(e){
  $('div.gnb_overlay').removeClass('show');
  $(this).find('div').removeClass('shown');
  $('#gnb > nav > ul > li.dropdown').removeClass('open');
  $('#gnb > nav > ul').removeClass('open');
  $('div.gnb_overlay').removeClass('show');
  $(this).find('div').removeClass('shown');
  $('#gnb > nav > ul > li.dropdown').removeClass('open');
  $('#gnb > nav > ul').removeClass('open');
});




/* Featured */
$(document).on('click', '#ms-idx-bnnr .anchor a', function(e){
  chgGrid();
  e.preventDefault();
});
var chgAction = function(idx){
    //트렌지션 지속 시간
    var delay = 600;
    var current = $('#ms-idx-bnnr div.grid:eq('+idx+')');
    var curContent = current.find('.box.current');
    var curImg = current.find('.grid_img a.current');
    var nextContent = current.find('.box:not(.current):eq(0)');
    var nextImg = current.find('.grid_img a:not(.current):eq(0)');
    var lastContent = current.find('.box:last');
    var lastImg = current.find('.grid_img a:last');

    curContent.removeClass('current').addClass('out');
    curImg.removeClass('current').addClass('out');
    nextContent.addClass('in');
    nextImg.addClass('in');
    setTimeout(function(){
      lastContent.after(curContent);
      curContent.removeClass('out');
      lastImg.after(curImg);
      curImg.removeClass('out');
      nextContent.addClass('current').removeClass('in');
      nextImg.addClass('current').removeClass('in');
    },delay);

  };
  var chgGrid = function(){
    //다음 gird 딜레이
    var delay = 200;
    var cnt = $('#ms-idx-bnnr div.grid').length;
    for(var i=0;i<cnt-1;i++){
      setTimeout(chgAction, i*delay, i);
    }
  };
  /* End of Featured */

  /* Search */
  $(function() {
    $('#hd_qnb_search').click(function() {
      if ($('#hd_sch').hasClass("visible")) {
        $('#hd_sch .close').removeClass('search-is-visible');
        $('#hd_sch').removeClass('visible');
        $('.gnb_overlay_search').removeClass('show');
      } else {
        $('#hd_sch .close').addClass('search-is-visible');
        $('#hd_sch').addClass('visible');
        $('.gnb_overlay_search').addClass('show');
        setTimeout(function() {
          $('#hd_sch_str').focus();
        },200);
      }
    });
    $('.gnb_overlay_search,#hd_sch .close').click(function() {
      $('#hd_sch .close').removeClass('search-is-visible');
      $('#hd_sch').removeClass('visible');
      $('.gnb_overlay_search').removeClass('show');
    });
  });

  /* Events */
  $(function() {
    $('#events').slidesjs({
      width: 599,
      height: 250,
      play: {
        active: true,
        effect: "fade",
        crossfade: "true",
        interval: 5000,
        auto: true,
        swap: true,
        restartDelay: 4000
      }
    });
  });


/* more */
$(function() {
  $('.more_button,.loader').click(function() {
    $('.plus').addClass('hidden');
    $('.loader').children('span').addClass('visible').removeClass('hidden');
  });
});